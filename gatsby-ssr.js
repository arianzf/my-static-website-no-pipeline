// Import React so that you can use JSX in HeadComponents
const React = require("react");

const divStyle = {
  right: '0',
  bottom: '0',
  position: 'absolute',
  'zIndex': 1,
  color: 'white'
};

const PostHeaderComponents = [
  <div style={divStyle}>Version: %%VERSION%%</div>
];

exports.onPreRenderHTML = ({ getHeadComponents, replaceHeadComponents }) => {
  const headComponents = getHeadComponents();
  replaceHeadComponents([...headComponents, ...PostHeaderComponents])
}